use std::mem::swap;

fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub unsafe fn move_zeroes(nums: &mut Vec<i32>) {
        let n = nums.len();
        let mut last = 0;
        for i in 0..n {
            if nums[i] != 0 {
                let temp = nums[i];
                nums[i] = nums[last];
                nums[last] = temp;
                last += 1;
            }
        }
    }
}