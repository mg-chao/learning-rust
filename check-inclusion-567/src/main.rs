use std::ops::Not;

fn main() {}
struct Solution;
impl Solution {
    pub fn check_inclusion(s1: String, s2: String) -> bool {
        let n1 = s1.len();
        let n2 = s2.len();
        if n1 > n2 {
            return false;
        }
        let mut b2 = Vec::with_capacity(n2);
        for b in s2.bytes() {
            b2.push((b - b'a') as usize)
        }
        let mut counts = vec![0usize; 26];
        for (i, c) in s1.bytes().enumerate() {
            counts[(c - b'a') as usize] -= 1;
            counts[b2[i]] += 1;
        }
        let mut diff_count = 0;
        for c in counts.iter() {
            if c != &0 {
                diff_count += 1;
            }
        }
        if diff_count == 0 {
            return true;
        }
        for i in n1..n2 {
            let c1 = b2[i];
            let c2 = b2[i - n1];
            if c1 == c2 {
                continue;
            }
            if c1 == 0 {
                diff_count += 1;
            }
            counts[c1] += 1;
            if c1 == 0 {
                diff_count -= 1;
            }
            if counts[c2] == 0 {
                diff_count += 1;
            }
            counts[c2] -= 1;
            if counts[c2] == 0 {
                diff_count -= 1;
            }
            if diff_count == 0 {
                return true;
            }
        }
        return false;
    }
}
