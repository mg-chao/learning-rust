fn main() {
    println!("Hello, world!");
}
fn find_nb(m: u64) -> i32 {
    let mut ans = 0;
    let mut volumn = 0;
    while volumn < m {
        volumn += ans * ans * ans;
        ans += 1;
    }
    return if volumn == m { (ans - 1) as i32 } else { -1 };
}
