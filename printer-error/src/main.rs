use std::fmt::format;

fn main() {
    println!("Hello, world!");
}
fn printer_error(s: &str) -> String {
    let mut count = 0;
    for c in s.chars() {
        if c > 'm' {
            count += 1;
        }
    }
    return format!("{}/{}", count, s.len());
}
