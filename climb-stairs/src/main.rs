fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn climb_stairs(n: i32) -> i32 {
        if n < 4 {
            return n;
        }
        let mut pre = 2;
        let mut cur = 3;
        let mut swap;
        for _ in 4..=n {
            swap = cur;
            cur += pre;
            pre = swap;
        }
        return cur;
    }
}
