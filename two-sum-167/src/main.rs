fn main() {
    Solution::two_sum(vec![2, 7, 11, 15], 9);
}
struct Solution;
impl Solution {
    pub fn two_sum(numbers: Vec<i32>, target: i32) -> Vec<i32> {
        // 小了移动左边，大了移动右边
        let mut left = 0;
        let mut right = numbers.len() - 1;
        while left < right {
            let sum = unsafe { numbers.get_unchecked(left) + numbers.get_unchecked(right) };
            if sum == target {
                return vec![left as i32 + 1, right as i32 + 1];
            } else if sum < target {
                left += 1;
            } else {
                right -= 1;
            }
        }
        return vec![-1, -1];
    }
}
