fn main() {
    let t = array_diff(vec![1, 2, 2], vec![2]);
    let a = t;
    let _ = a.first();
}
use std::{collections::HashSet, hash::Hash, iter::FromIterator};
fn array_diff<T: PartialEq + Copy + Eq + Hash>(a: Vec<T>, b: Vec<T>) -> Vec<T> {
    let set = HashSet::<T>::from_iter(b);
    let mut ans = Vec::with_capacity(a.len());
    for n in a {
        if !set.contains(&n) {
            ans.push(n);
        }
    }
    return ans;
}
