fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn fib(n: i32) -> i32 {
        if n < 2 {
            return n;
        }
        let mut pre = 0;
        let mut cur = 1;
        let mut swap;
        for _ in 2..=n {
            swap = cur;
            cur += pre;
            pre = swap;
        }
        return cur;
    }
}
