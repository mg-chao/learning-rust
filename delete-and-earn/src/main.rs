fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn delete_and_earn(nums: Vec<i32>) -> i32 {
        let n = *nums.iter().max().unwrap() as usize;
        let mut counts = vec![0; n + 1];
        for i in nums {
            counts[i as usize] += 1;
        }
        let mut dp = vec![0; n + 1];
        dp[1] = counts[1];
        for i in 2..=n {
            dp[i] = dp[i - 1].max(dp[i - 2] + counts[i] * i);
        }
        return dp[n] as i32;
    }
}
