fn main() {
    anagrams("abba", &["aabb".to_string(), "abcd".to_string(), "bbaa".to_string(), "dada".to_string()]);
}
use std::collections::HashMap;
fn anagrams(word: &str, words: &[String]) -> Vec<String> {
    let mut ans = vec![];
    let mut ls: HashMap<u8, usize> = HashMap::new();
    for i in word.bytes() {
        if let Some(k) = ls.get_mut(&i) {
            *k += 1;
        } else {
            ls.insert(i, 1);
        }
    }
    for word in words {
        let mut set: HashMap<u8, usize> = HashMap::new();
        for i in word.bytes() {
            if let Some(k) = set.get_mut(&i) {
                *k += 1;
            } else {
                set.insert(i, 1);
            }
        }
        let mut is_anagrams = true;
        for (k, v) in set.iter() {
            if !ls.contains_key(k) || ls.get(k) != Some(v) {
                is_anagrams = false;
                break;
            }
        }
        if is_anagrams {
            ans.push(word.to_owned())
        }
    }
    return ans;
}
