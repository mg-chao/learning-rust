fn main() {
    Solution::search(vec![5], -5);
}
struct Solution;
use std::cmp::Ordering::*;
impl Solution {
    pub fn search(nums: Vec<i32>, target: i32) -> i32 {
        let mut size = nums.len();
        let mut left = 0;
        let mut right = size;
        while left < right {
            let mid = left + size / 2;
            let cmp = unsafe { nums.get_unchecked(mid) }.cmp(&target);
            if cmp == Less {
                left = mid + 1;
            } else if cmp == Greater {
                right = mid;
            } else {
                return mid as i32;
            }
            size = right - left;
        }
        return -1;
    }
}
