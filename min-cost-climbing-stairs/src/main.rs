fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn min_cost_climbing_stairs(cost: Vec<i32>) -> i32 {
        let n = cost.len();
        let mut mins = Vec::with_capacity(n);
        mins.push(cost[0]);
        mins.push(cost[1]);
        for i in 2..n {
            mins.push((cost[i] + mins[i - 1]).min(cost[i] + mins[i - 2]));
        }
        return mins[n - 1].min(mins[n - 2]);
    }
}
