fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn rob(nums: Vec<i32>) -> i32 {
        let n = nums.len();
        let mut maxs = Vec::with_capacity(n + 1);
        maxs.push(0);
        maxs.push(nums[0]);
        for i in 1..n {
            maxs.push(maxs[i].max(maxs[i - 1] + nums[i]));
        }
        return maxs[n].max(maxs[n - 1]);
    }
}