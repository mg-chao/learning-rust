fn main() {
    println!("Hello, world!");
}
fn solution(word: &str, ending: &str) -> bool {
    word.ends_with(ending)
}