fn main() {
    println!("Hello, world!");
}
// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}
struct Solution;
impl Solution {
    pub fn remove_nth_from_end(head: Option<Box<ListNode>>, n: i32) -> Option<Box<ListNode>> {
        let mut ans = Some(Box::new(ListNode::new(0)));
        ans.as_mut().unwrap().next = head;
        let mut fast = ans.clone();
        let mut slow = &mut ans;
        for _ in 0..=n {
            fast = fast.unwrap().next;
        }
        while let Some(fast_node) = fast {
            fast = fast_node.next;
            slow = &mut slow.as_mut().unwrap().next;
        }
        slow.as_mut().unwrap().next = slow.as_mut().unwrap().next.clone().unwrap().next;
        return ans.unwrap().next;
    }
}
