fn main() {
    accum("ACDCASDDS");
}

fn accum(s: &str) -> String {
    let n = s.len();
    // 结果的长度必然为 (1 + n) / 2 * n + (n - 1)
    // 为了方便计算，结尾保留一位填充 '-'
    // 如 A-Bb-
    let mut ans = String::with_capacity((4 + n) / 2 * n);
    let mut count = 0;
    for c in s.chars() {
        ans.push(c.to_ascii_uppercase());
        for _ in 0..count {
            ans.push(c.to_ascii_lowercase());
        }
        ans.push('-');
        count += 1;
    }
    ans.pop();
    return ans;
}
