fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn tribonacci(n: i32) -> i32 {
        if n < 2 {return n};
        let mut cur = 1;
        let mut pre = 0;
        let mut mpre = 0;
        let mut swap;
        for _ in 2..=n {
            swap = cur;
            cur += pre + mpre;
            mpre = pre;
            pre = swap;
        }
        return cur;
    }
}