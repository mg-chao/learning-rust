fn main() {
    println!("Hello, world!");
}
struct Solution;
impl Solution {
    pub fn reverse_string(s: &mut Vec<char>) {
        let mut left = 0;
        let mut right = s.len();
        while left < right {
            let swap = s[left];
            s[left] = s[right];
            s[right] = swap;
            left += 1;
            right -= 1;
        }
    }
}
