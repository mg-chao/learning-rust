fn main() {
    println!("Hello, world!");
}
fn product_fib(prod: u64) -> (u64, u64, bool) {
    let mut n = 1;
    let mut pre = 0;
    let mut temp;
    while n * pre < prod {
        temp = pre;
        pre = n;
        n += temp;
    }
    return (pre, n, n * pre == prod);
}
