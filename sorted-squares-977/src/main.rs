fn main() {
    println!("{:?}",Solution::sorted_squares(vec![1]));
}

struct Solution;
impl Solution {
    pub fn sorted_squares(mut nums: Vec<i32>) -> Vec<i32> {
        let n = nums.len();
        // 计算平方
        for n in nums.iter_mut() {
            *n = n.pow(2);
        }
        let mut ans = vec![0; n];
        let mut cur = n - 1;
        let mut base = 0;
        let mut size = cur;
        unsafe {
            while size != 0 {
                let left = *nums.get_unchecked(base);
                let right = *nums.get_unchecked(base + size);
                if left > right {
                    ans[cur] = left;
                    base += 1;
                } else {
                    ans[cur] = right;
                }
                size -= 1;
                cur -= 1;
            }
            ans[0] = *nums.get_unchecked(base);
        }
        return ans;
    }
}
