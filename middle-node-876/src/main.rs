fn main() {
    println!("Hello, world!");
}
// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}
struct Solution;
impl Solution {
    pub fn middle_node(head: Option<Box<ListNode>>) -> Option<Box<ListNode>> {
        let mut slow = head.clone();
        let mut fast = head.unwrap().next;
        while let Some(fast_node) = fast {
            fast = fast_node.next;
            slow = slow.unwrap().next;
            if let Some(fast_node) = fast {
                fast = fast_node.next;
            }
        }
        return slow;
    }
}
