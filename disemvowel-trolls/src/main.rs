fn main() {
    println!("Hello, world!");
}

fn disemvowel(s: &str) -> String {
    let mut ans = String::with_capacity(s.len());
    for c in s.chars() {
        match c {
            'a'  | 'e' | 'i' |'o' |'u' |
            'A'  | 'E' | 'I' |'O' |'U'
                => continue,
            _ => ans.push(c)
        }
    }
    return ans;
}
