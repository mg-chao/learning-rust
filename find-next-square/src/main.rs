fn main() {
    let t =find_next_square(15_241_383_936);
    if let Some(t) = t {
        println!("{}",t);
    }
}

fn find_next_square(sq: u64) -> Option<u64> {
    if sq == 0 {
        return Some(1);
    }
    let mut sq = sq as u128;
    let c = sq as u128;
    let mut sqi ;
    loop {
        sqi = (sq + c / sq) >> 1;
        match (sqi * sqi).cmp(&c) {
            std::cmp::Ordering::Less => return None,
            std::cmp::Ordering::Equal => {
                sqi += 1;
                return Some((sqi * sqi) as u64);
            },
            std::cmp::Ordering::Greater => sq = sqi,
        }
    }
}
